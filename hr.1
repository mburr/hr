.\" Copyright (c) June 2014 Frank J Leonhardt
.\"	All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\"
.Dd June, 22, 2014
.Dt HR 1 CON
.Os
.Sh NAME
.Nm hr
.Nd Format numbers in human-readable form 
.Sh SYNOPSIS
.Nm
.Op Fl b
.Op Fl p
.Op Fl f Ns Ar field
.Op Fl s Ns Ar bits
.Op Fl w Ns Ar width
.Op Ar 
.Sh DESCRIPTION 
The
.Nm
utility formats numbers taken from the input stream and sends them to stdout
in a format that's human readable. Specifically, it scales the number and
adds an appropriate suffix (e.g. 1073741824 becomes 1.0M) 
.Pp
The options are as follows:
.Bl -tag -width indent
.It Fl b
Put a 'B' suffix on a number that hasn't been scaled (for Bytes)
.It Fl p
Attempt to deal with input fields that have been padded with spaces for formatting
purposes.
.It Fl w Ns Ar width
Set the field width to 
.Ar field 
characters. The default is four (three digits and
a suffix). Widths less than four are not normally useful.
.It Fl s Ns Ar bits
Shift the number being processed right by
.Ar bits
bits. i.e. multiply by 2^bits. This is useful if the number has already
been scaled in to units. For example, if the number is in 512-byte blocks
then
.Fl s Ns Ar 9
will multiply the output number by 512 before scaling it. If the number
was already in Kb use
.Fl s Ns Ar 10
and so on. In addition to specifying the number of bits to shift as a number
you may also use one of the SI suffixes B, K, M, G, T, P, E (upper or lower case).
.It Fl f Ns Ar field
Process the number in the numbered 
.Ar field
, with fields being numbered from 0 upwards and separated by whitespace.
.El
.Pp
The
.Nm
utility currently uses the humanize() function in
.Lb libutil
to format the numbers. 
This will repeatedly divide the input number by 1024 until it fits in
to a width of three digits (plus suffix), unless the width is modified by the 
.Fl w
option. Depending on the number of divisions required it will append a k, M, G, 
T, P or E suffix as appropriate. If the
.Fl b 
option is specified it will append a 'B' if no division is required.
.Pp
If no file names are specified,
.Nm
will get its input from stdin. If '-' is specified as one of the file names
.Nm
will read from stdin at this point.
.Pp
If you wish to convert more than one field, simply pipe the output from one
.Nm
command into another.
.Pp
By default the first field (i.e. field 0) is converted, if possible, and the 
output will be four characters wide including the suffix.
.Pp
If the field being converted contains non-numeral characters they will be passed
through unchanged.
.Pp
Command line options may appear at any point in the line, and will only take 
effect from that point onwards. This allows different options
to apply to different input files. You may cancel an option by prepending it 
with a '-'. For consistency, you can also set an option explicitly with a '+'.
Options may also be combined in a string. For example:
.Pp
.Dl hr -b Ar file1 -b- Ar file2
.Pp
Will add a 'B' suffix when processing file1 but cancel it for file2.
.Pp
.Dl hr -bw5f4p Ar file1
.Pp
Will set the B suffix option, set the output width to 5 characters, process field 4
and remove excess padding from in front of the original digits.
.Pp
.Sh EXAMPLES
.Bl -tag -width indent
To format the output of an ls -l command's file size use:
.Pp
.Dl ls -l | hr -p -b -f4
.Pp
This output will be very similar to the output of "ls -lh" using these options. However
the -h option isn't available with the -ls option on the "find" command. You can use
this to achieve it:
.Pp
.Dl find . -ls | hr -p -f6
.Pp
Finally, if you wish to produce a sorted list of directories by size in human format, try:
.Pp
.Dl du -d1 | sort -n | hr -s10
.Pp
This assumes that the output of du is the disk usage in kilobytes, hence the need for the 
.Fl s Ns Ar 10
.Sh DIAGNOSTICS
.Ex -std hr
.Sh SEE ALSO
.Xr humanize_number 3
.Sh BUGS
.Nm
uses the humanize_number() utility function to ensure it is consistent with other FreeBSD
utilities in its output. It is therefore subject to the limitations of this function. In 
particular, as this takes a 64-bit integer, it cannot convert any numbers greater than this
and will wrap-around should you try to convert one. There is another version (0.1) that 
implements its own incremental conversion routine and can go into Zetabytes, but this isn't it.
I plan to merge the two and switch to the alternative method if humanize_number() cannot cope.
This is planned for release 1.0.

.Sh HISTORY
The
.Nm
command has yet to appear in any release. It was written for FreeBSD but should compile
on other BSD systems and Debian 7.7 and other Linux distributions using the BSD library.
.Pp
This is pre-release version 0.2
.Sh AUTHORS
The
.Nm
command was written by 
.An "Frank J Leonhardt" Aq freebsd@fjl.co.uk
www.fjl.co.uk, and has yet to appear.


