/*-
 * hr.c (c) 2014 Frank J Leonhardt. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


//#include <locale.h>

/* The lines below will load a BSD version number macro from sys/params.h
*  This is the only reason it is included. BSD is used later on to
*  decided if libutil.h is part of the system, or whether to try
*  and load it from the Linux BSD library.
*/
#if (defined(__unix__) || defined(unix)) && !defined(USG)
#include <sys/param.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <ctype.h>
#include <assert.h>

/* libutil.h is included for humanize_number() - not a great
*  function but its output is standard. It's been in BSD for a while
*  and in Debian Linux (at least) as a BSD Library function.
*/
 
#if defined(BSD)
#include <libutil.h>
#endif

#ifdef __linux__
/* Try and include it where it should exist on Debian 7. */
#include <bsd/stdlib.h>
#endif

/* Okay, we've tried to load the appropriate include file for humanize_number(). if your
*  System isn't covered by the above, you'll have to find it yourself
*/


static int process_file (char *filename, int b_flag, int p_flag, int field, int shift,int width);
static int num_from_arg (char * arg, int *ip);
static int arg_flag(char *arg, int *ip);

static void usage (char *name)
{
	fprintf(stderr,"Usage: %s [-b] [-p] [-f<field-width>]"
			" [-s<shift-bits>] [-f<field>] [<file> ...]\nThis is pre-release version 0.2\n",
							name);
	exit(1); 
}

int main (int argc, char **argv)
{
int i;
int field=0;	// Number of whitespace to skip before converting number
int b_flag=0;	// True if 'B' (for bytes) printed after number <999 (i.e. not scaled). Set by -b switch
int p_flag=0;	// True if option set to deal with whitespace padding in format (if possible)
int shift=0;	// Bits to shift number due to pre-scaling - e.g. if already in Kb, shift 10 bits.
int width=4;	// Width of human-readable output in characters. 
int processed_file = 0;	// True once at least one file processed (otherwise process stdin)
 
	for (i=1;i<argc;i++)
		if (argv[i][0] == '-')
		{
		int c,j = 1;

			while (c = argv[i][j++])	// Allow for multiple flags following '-'
			{
				switch (c)
				{
				case 's': 
					if (isdigit(argv[i][j]))
						shift = num_from_arg (argv[i],&j); 
					else
						switch(argv[i][j++] & 0x5f)
						{
						case 'B': shift = 0; break;
						case 'K': shift = 10; break;
						case 'M': shift = 20; break;
						case 'G': shift = 30; break;
						case 'T': shift = 40; break;
						case 'P': shift = 50; break;
						case 'E': shift = 60; break;
						default: 
							fprintf(stderr,"%s: -s option must be followed by digits to shift or one of B, K, M, G, T, P or E\n",argv[0]);
							usage(argv[0]); /* NOT REACHED */
						}
					break;

				case 'f': field = num_from_arg (argv[i],&j); break;
				case 'w': width = num_from_arg (argv[i],&j); break;
				case 'p': p_flag = arg_flag(argv[i],&j); break;
				case 'b': b_flag = arg_flag(argv[i],&j); break;
				default: fprintf(stderr,"%s: Invalid option  -- '%c'\n",argv[0],c);
					usage (argv[0]);
				}
			}
			if (j == 2)	// Argument was a '-' followed by NUL
				if (process_file ("-", b_flag, p_flag, field,shift, width))
					return 1;
		}
		else
		{
			if (process_file (argv[i], b_flag, p_flag, field,shift,width))
				return 1;
			processed_file = 1;
		}

	if (!processed_file)	// No files were given as arguments so do 'stdin'
		return process_file ("-", b_flag, p_flag, field, shift, width);
	return 0;
}


/* Return the value of an argument "flag". 
*	e.g. for argument -x:
*	-x =  true
*	-x- = false
*	-x+ = true (for consistency)
*	Advance pointer to argument string pointed to by *ip as needed. 
*/
 
static int arg_flag(char *arg, int *ip)
{
	if (arg[*ip] == '-')	// Turn OFF the flag
	{
		(*ip)++;
		return 0;
	}

	if (arg[*ip] == '+')	// Turn on with +, for consistency
		(*ip)++;

	return 1;		// Unless turning off, we always turn on
}



/* Return number represented by decimal digits
*  in argument string at offset *ip. Advance *ip as needed.
*
*   e.g. If argument was -n321b return 321 and leave *ip
*        pointing at 'b'
*/

static int num_from_arg (char * arg, int *ip)
{
long ret = 0;
int i = *ip;
int c;
	while ((c = arg[i]) >= '0' && c <= '9')
	{
		ret = ret * 10 + c - '0';
		i++;
	}
	*ip=i;
	return ret;
}


/* This is where we process individual files.
*  If the filename is '-' use stdin instead.
*  Return 0 if all okay, otherwise 1 if there was a problem.
*  Error messages will be printed to stderr first.
*  If there is a problem writing to stdout, print error and abort
*  entire program.
*/

static int process_file (char *filename, int b_flag, int p_flag, int field, int shift, int width)
{
FILE *fp; 		// Input stream
char c;			// Character being processed
int current_field = 0;	// Number of the whitespace-separated field currently being read
int in_space = 0;	// Flag to show we're skipping whitespace
int humanize_flags = HN_NOSPACE | HN_DECIMAL | (b_flag?HN_B:0);

	if (filename[0] == '-' && filename[1] == 0)	// stdin!
	{
		fp = stdin;
		if (feof (fp))		// We don't want an error before we start on on stdin
			clearerr(fp);
	}
	else
	{
		if (!(fp = fopen(filename,"r")))
		{
	                warn("%s", filename);
                	return 1;
		}
	}

	while ((c = getc(fp)) != EOF)
	{
		if (c == '\n')
		{
			current_field = 0;
			in_space = 1;
		}
		else
		{
			if (isspace(c) && !in_space)		// This set of conditions needs optimising when I'm more awake
			{
				in_space = 1;
				current_field++;
			}
			else
				if (isspace(c) && p_flag && field == current_field)
					continue;

			if (!isspace(c) && in_space)
			{
				in_space = 0;
			}

		}

		if (field == current_field && !in_space && isdigit(c))
		{
		int64_t number = 0;
		char buf[32];
		int len;
			assert (width <= 31);
		 	while (isdigit(c))
			{	
				number = number * 10 + c - '0';
			 	c = getc(fp);	// (EOF is valid end and will test again at TOL)
			}
	
			if ((len = humanize_number (buf, width+1, number<<shift, "", HN_AUTOSCALE, humanize_flags))> 0)
			{
				len = width-len;
				while (len--)
					putchar(' ');
			}
		 	if (fputs(buf,stdout) == EOF)
				break;

			if (isspace(c))
			{
				if (c == '\n')
					current_field = 0;
				else
					current_field++;
				in_space = 1;
			}
		}

		if (putchar(c) == EOF)
			break;
	}

        if (ferror(fp)) 
	{
               warn("%s", filename);
               clearerr(fp);
        }

	if (fp != stdin)
		fclose (fp);

        if (ferror(stdout))
	{
                err(1, "stdout");
		/* Not reached */
	}
	return 0;
}

/* end */

